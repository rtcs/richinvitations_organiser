package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.databinding.ContactsListItemBinding;
import com.richtree.richinvitations.organizer.model.ContactsModel;

import java.util.ArrayList;

/**
 * Created by richtree on 11/15/2018.
 */

public class ImportContacts_Sheet_Adapter extends BaseAdapter {
    Context context;
    ArrayList<ContactsModel> contactslist = new ArrayList<>();
    boolean ischeckboxenabled;


    public ImportContacts_Sheet_Adapter(Context context, ArrayList<ContactsModel> contactslist, boolean ischeckboxenabled) {
        this.context = context;
        this.contactslist = contactslist;
        this.ischeckboxenabled = ischeckboxenabled;
    }

    @Override
    public int getCount() {
        return contactslist.size();
    }

    @Override
    public Object getItem(int position) {
        return contactslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ContactsListItemBinding binding;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.contacts_list_item, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ContactsListItemBinding) convertView.getTag();
        }

        if (ischeckboxenabled == true) {
            binding.check.setVisibility(View.VISIBLE);
        }else{
            binding.check.setVisibility(View.GONE);
        }

        binding.setContacts(contactslist.get(position));

        return binding.getRoot();
    }

}
