package com.richtree.richinvitations.organizer.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.richtree.richinvitations.organizer.controllers.UserController;
import com.richtree.richinvitations.organizer.model.ViewModel.User;
import com.richtree.richinvitations.organizer.utils.APIResponse;
import com.richtree.richinvitations.organizer.utils.Utils;
import com.richtree.richinvitations.organizer.view.LoginView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 28/11/2018.
 */

public class LoginPresenter implements APIResponse {
    private final LoginView loginView;


    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    public void getUserName() {
        String message = Utils.isInputValid(loginView.getUserName());
        if (!message.isEmpty()) loginView.showError(message);
        else loginUser(Utils.isPhonenumberOrEmail(loginView.getUserName()));

    }

    public void loginUser(String username) {
        JSONObject object = new JSONObject();
        try {
            object.put("email", username);
            object.put("password", "user@123");
            UserController.loginUser(object.toString(), this);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void success(String response) {
        Log.d("response", response);
        Gson gson = new Gson();
        User user = gson.fromJson(response, User.class);
        loginView.navigateToMainActivity();


    }

    @Override
    public void fail(String error) {
        loginView.showError(error);
    }

    @Override
    public void accessTokenExpired() {
        loginUser(Utils.isPhonenumberOrEmail(loginView.getUserName()));
    }
}
