package com.richtree.richinvitations.organizer.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.PastInvitationsAdapter;
import com.richtree.richinvitations.organizer.databinding.FragmentPastInvitationsBinding;
import com.richtree.richinvitations.organizer.model.Order;
import com.richtree.richinvitations.organizer.presenter.PastInvitationsPresenter;
import com.richtree.richinvitations.organizer.utils.Utils;
import com.richtree.richinvitations.organizer.view.PastInvitationsView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PastInvitations extends Fragment implements PastInvitationsView {
    FragmentPastInvitationsBinding binding;
    View view;
    PastInvitationsPresenter pastInvitationsPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_past_invitations, container, false);
        view = binding.getRoot();
        pastInvitationsPresenter = new PastInvitationsPresenter(this);
        pastInvitationsPresenter.getPastOrders();
        return view;
    }

    @Override
    public void getPastOrders(ArrayList<Order> past_orders) {
        if (past_orders.size() > 0) {
            PastInvitationsAdapter adapter = new PastInvitationsAdapter(getActivity(), past_orders);
            binding.invite.setAdapter(adapter);
        }

    }


    @Override
    public void displayError(String error) {
        Utils.showToast(error);

    }

    @Override
    public void success(String success) {

    }
}
