package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;


/**
 * Created by Admin on 2017-12-01.
 */

public class MainGridAdapter extends BaseAdapter {

  Context context;

  private static String[] gridname = {
          "Invitations", "Guest List",
          "Reports", "Imports",
          "Settings", "Notifications"
  };

  private static int[] gridimage = {
          R.drawable.invitations, R.drawable.guest_list,
          R.drawable.report, R.drawable.imports,
          R.drawable.settings, R.drawable.notification
  };

  private static int[] colors = {
          R.color.white, R.color.white,
          R.color.white, R.color.white,
          R.color.white, R.color.white
  };


  public MainGridAdapter(Context context) {
    this.context = context;
  }

  @Override
  public int getCount() {
    // TODO Auto-generated method stub
    return gridname.length;
  }

  @Override
  public Object getItem(int position) {
    // TODO Auto-generated method stub
    return gridname[position];
  }

  @Override
  public long getItemId(int position) {
    // TODO Auto-generated method stub
    return position;
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    // TODO Auto-generated method stub

    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.gridview_list, parent, false);
    }

    ImageView img = (ImageView) convertView.findViewById(R.id.imageView);
    TextView name = (TextView) convertView.findViewById(R.id.textView);
    LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.llayout);

    img.setImageResource(gridimage[position]);
    name.setText(gridname[position]);
    layout.setBackgroundResource(colors[position]);

    return convertView;
  }

}