package com.richtree.richinvitations.organizer.controllers;

import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.utils.APIResponse;
import com.richtree.richinvitations.organizer.utils.RetrofitCallBack;

/**
 * Created by admin on 26/03/2018.
 */

public class UserController {
    public static void loginUser(String request, APIResponse apiResponse) {
        MyApplication.returnInstance().userService.loginUser(request).enqueue(new
                RetrofitCallBack(apiResponse));

    }


}
