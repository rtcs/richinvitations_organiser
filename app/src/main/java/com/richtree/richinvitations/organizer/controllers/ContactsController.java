package com.richtree.richinvitations.organizer.controllers;

import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.interfaces.BaseRetrofitCallback;
import com.richtree.richinvitations.organizer.utils.ResponseCallback;

/**
 * Created by admin on 01/12/2018.
 */

public class ContactsController {
    public static void uploadBulkCuustomers(MyApplication myApplication, String request,
                                            final BaseRetrofitCallback callback) {
        myApplication.contactsService.bulkUploadContacts(request).enqueue(new ResponseCallback
                (myApplication, callback));
    }
}
