package com.richtree.richinvitations.organizer.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.model.Guest;

public class GuestDetails extends AppCompatActivity {
    private static final String[] ITEMSGROUP = {"Additional Details"};
    private static final String[] ITEMS = {"Hotel Name", "Room No.", "Driver Name", "Driver Mobile #", "Car No."};

    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapter2;

    MaterialSpinner spinner1;
    MaterialSpinner spinner2;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_details);
        setTitle("Guest Details");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinner1 = (MaterialSpinner) findViewById(R.id.spinner);
        spinner2 = (MaterialSpinner) findViewById(R.id.spinner2);
        editText = (EditText) findViewById(R.id.description);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMSGROUP);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        initSpinnerMultiline();
        initSpinnerMultiline2();
    }

    private void initSpinnerMultiline() {
        spinner1.setAdapter(adapter);
        spinner1.setHint("Select Attribute Group");
    }

    private void initSpinnerMultiline2() {
        spinner2.setAdapter(adapter2);
        spinner2.setHint("Select Attribute");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InvitationDetails.class);
        startActivity(intent);
        finish();
    }
}
