package com.richtree.richinvitations.organizer.view;

import com.richtree.richinvitations.organizer.model.Order;

import java.util.ArrayList;

/**
 * Created by admin on 09/12/2018.
 */

public interface UpcomingInvitationsview extends BaseView {
    void getUpcomingInvitations(ArrayList<Order> past_orders);


}
