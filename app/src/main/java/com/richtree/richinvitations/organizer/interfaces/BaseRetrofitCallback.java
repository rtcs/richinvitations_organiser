package com.richtree.richinvitations.organizer.interfaces;

/**
 * Created by TGT on 3/29/2018.
 */

public interface BaseRetrofitCallback {
  void success(String success);

  void fail(String fail);
}
