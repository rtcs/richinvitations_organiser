package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.databinding.ImportContactListItemBinding;
import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.presenter.ContactSelectionPresenter;
import com.richtree.richinvitations.organizer.view.ContactCheckedCallBack;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public class ImportContacts_Adapter extends BaseAdapter implements ContactCheckedCallBack {
    Context context;
    ContactSelectionPresenter contactSelectionPresenter;
    ImportContactListItemBinding binding;
    ArrayList<ContactsModel> contactslist = new ArrayList<>();
    getSelectedContactsCallback getSelectedContactsCount;


    public ImportContacts_Adapter(Context context, ArrayList<ContactsModel> contactslist,
                                  getSelectedContactsCallback getSelectedContacts) {
        this.context = context;
        this.contactslist = contactslist;
        this.getSelectedContactsCount = getSelectedContacts;
        contactSelectionPresenter = new ContactSelectionPresenter(this);


    }

    public interface getSelectedContactsCallback {
        void getSelected(ArrayList<ContactsModel> selectedcontactslist);
    }

    @Override
    public int getCount() {
        return contactslist.size();
    }

    @Override
    public Object getItem(int position) {
        return contactslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.import_contact_list_item,
                    null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ImportContactListItemBinding) convertView.getTag();
        }

        binding.setContacts(contactslist.get(position));
        binding.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                contactSelectionPresenter.addOrRemoveContact(contactslist.get(position));

            }
        });

        return binding.getRoot();
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public void iscontactChecked(boolean ischeck) {
        if (!ischeck) {
            binding.checkbox.setChecked(false);
        } else {
            binding.checkbox.setChecked(true);
        }

    }

    @Override
    public void SelectedContacts(ArrayList<ContactsModel> selectedcontactslist) {
        getSelectedContactsCount.getSelected(selectedcontactslist);
    }


}
