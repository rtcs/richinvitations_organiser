package com.richtree.richinvitations.organizer.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.databinding.ActivityLoginBinding;
import com.richtree.richinvitations.organizer.presenter.LoginPresenter;
import com.richtree.richinvitations.organizer.view.LoginView;

public class LoginActivity extends BaseActivity implements LoginView, View.OnClickListener {
    Button login;
    LoginPresenter loginPresenter;
    TextView already;
    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

    }

    void init() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginPresenter = new LoginPresenter(this);
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public String getUserName() {
        return binding.etInput.getText().toString();
    }


    @Override
    public void showError(String error) {
        showToast(error);
        hideProgressDialog();


    }

    @Override
    public void navigateToMainActivity() {
        hideProgressDialog();
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                showProgressDialog();
                loginPresenter.getUserName();
                break;

        }
    }


}
