package com.richtree.richinvitations.organizer.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.GuestListAdapter;
import com.richtree.richinvitations.organizer.interfaces.CheckClickInterface;
import com.richtree.richinvitations.organizer.model.Guest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.Manifest.permission.READ_CONTACTS;

public class AddGuestList extends AppCompatActivity implements CheckClickInterface {
    private static final int REQUEST_READ_CONTACTS = 444;
    private ListView mListView;
    private ProgressDialog pDialog;
    private android.os.Handler updateBarHandler;

    ArrayList<Guest> contactList = new ArrayList<Guest>();
    ArrayList<Guest> attachList = new ArrayList<Guest>();
    Cursor cursor;
    int counter;

    String encodedImage = "";
    String jSonValues;
    String phoneNumber = null;

    Button view, count, save, dialogButtonOK;
    TextView tx_jsonResult;
    ImageView add_contact;

    private static final String[] PROJECTION = new String[] {
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    };

    private CheckClickInterface checkClickInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_guest_list);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add Guest List");

        pDialog = new ProgressDialog(AddGuestList.this);
        pDialog.setMessage("Reading contacts...");
        pDialog.setCancelable(false);
        pDialog.show();

        mListView = (ListView) findViewById(R.id.guest_list);
        updateBarHandler = new android.os.Handler();

        view = (Button) findViewById(R.id.view);
        count = (Button) findViewById(R.id.total);
        save = (Button) findViewById(R.id.save);

        // Since reading contacts takes more time, let's run it on a separate thread.
        new Thread(new Runnable() {

            @Override
            public void run() {
                getContacts();
            }
        }).start();
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContacts();
            }
        }
    }

    public void getContacts() {

        if (!mayRequestContacts()) {
            return;
        }

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;


        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String email_data = ContactsContract.CommonDataKinds.Email.DATA;

        StringBuffer output;

        ContentResolver contentResolver = getContentResolver();
        cursor = contentResolver.query(CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                output = new StringBuffer();

                // Update the progress message
                updateBarHandler.post(new Runnable() {
                    public void run() {
                        pDialog.setMessage("Reading contacts : " + counter++ + "/" + cursor.getCount());
                    }
                });

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {
                    output.append("First Name:" + name);
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                    if (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append("\nPhone number:" + phoneNumber);
                    }



                    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ri_icon_n);
                    boolean flag = false;
                    try {
                        InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contact_id)));

                        if (inputStream != null) {
                            bm = BitmapFactory.decodeStream(inputStream);
                            flag = true;
                            assert inputStream != null;
                            inputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    encodedImage = "";
                    if (flag) {
                        if (bm != null) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byte[] image = stream.toByteArray();
                            encodedImage = Base64.encodeToString(image, Base64.DEFAULT);
                        }
                        Log.d("Image", ">" + encodedImage);
                    }
                }
                Guest guest = new Guest();
                guest.setName(name);
                guest.setPhoneNumber(phoneNumber);
                guest.setImage(encodedImage);
                // Add the contact to the ArrayList
                contactList.add(guest);
            }

            // ListView has to be updated using a ui thread
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    GuestListAdapter adapter = new GuestListAdapter(AddGuestList.this, contactList, AddGuestList.this);
                    mListView.setAdapter(adapter);
                }
            });
            // Dismiss the progressbar after 500 millisecondds
            updateBarHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    pDialog.cancel();
                }
            }, 500);
        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (attachList.size() > 0) {
                    for (int i = 0; i < attachList.size(); i++) {
                    }
                    Intent intent = new Intent(AddGuestList.this, SelectedGuestList.class);
                    intent.putExtra("list", attachList);
                    startActivity(intent);
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddGuestList.this);
                    builder.setTitle("Your Contacts List");
                    builder.setMessage("Plz Select The Contacts....");
                    builder.setCancelable(false);

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.cancel();

                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    // Toast.makeText(ContactsList.this, "" + attachList.size(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSONObject jo = new JSONObject();
                JSONArray ja = new JSONArray();

                for (int i = 0; i < attachList.size(); i++) {
                    JSONObject jGroup = new JSONObject();

                    try {
                        jGroup.put("personName ", attachList.get(i).getName());
                        jGroup.put("personPhoneNumber ", attachList.get(i).getPhoneNumber());
                        jGroup.put("email ", attachList.get(i).getPhoneNumber() + "@richevents.in");
                        ja.put(jGroup);
                        jo.put("Contacts Details", ja);

                        // Toast.makeText(getApplicationContext(),"JsonObject" + jo,Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    jSonValues = jo.toString();

                }

                showDialog();


            }

            private void showDialog() {


                final Dialog dialog = new Dialog(AddGuestList.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dialog);

                tx_jsonResult = (TextView) dialog.findViewById(R.id.jsonResults);
                dialogButtonOK = (Button) dialog.findViewById(R.id.dialogButtonOK);

                tx_jsonResult.setText(jSonValues);

                dialogButtonOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void clickListener(CheckBox view, Guest guest) {

        if (view.isChecked()) {
            guest.setChecked(true);
            attachList.add(guest);

            if (attachList.size() > 0) {

                int size = attachList.size();
                count.setText("Total  :  " + String.valueOf(size));

            } else {
                count.setText("Total");
            }
            //Toast.makeText(this, "Checked" + attachList.size(), Toast.LENGTH_SHORT).show();
        } else {

            guest.setChecked(false);
            attachList.remove(guest);

            if (attachList.size() > 0) {

                int size = attachList.size();
                count.setText("Total  :  " + String.valueOf(size));


            } else {
                count.setText("Total");
            }

            // Toast.makeText(this, "Unchecked" + attachList.size(), Toast.LENGTH_SHORT).show();
        }
    }

}
