package com.richtree.richinvitations.organizer.presenter;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.controllers.ContactsController;
import com.richtree.richinvitations.organizer.interfaces.BaseRetrofitCallback;
import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.view.ContactCheckedCallBack;
import com.richtree.richinvitations.organizer.view.ImportContactsView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public class ImportContactsPresenter implements BaseRetrofitCallback {
    ImportContactsView importContactsView;
    ArrayList<ContactsModel> selectedcontactslist = new ArrayList<>();
    ArrayList<ContactsModel> contactslist = new ArrayList<>();
    ContactCheckedCallBack contactCheckedCallBack;
    int count = 0;
    private static final String[] PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER

    };

    public ImportContactsPresenter(ImportContactsView importContactsView) {
        this.importContactsView = importContactsView;
    }

    public ImportContactsPresenter(ContactCheckedCallBack contactCheckedCallBack) {
        this.contactCheckedCallBack = contactCheckedCallBack;
    }


    public void readContacts(final ContentResolver contentResolver) {
        contactslist = new ArrayList<>();
        Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION,
                null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");
        if (cursor != null) {
            try {
                final int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                final int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);


                String name, number, email;
                while (cursor.moveToNext()) {
                    name = cursor.getString(nameIndex);
                    number = cursor.getString(numberIndex);
                    ContactsModel contact = new ContactsModel();
                    contact.setFirstname(name);
                    contact.setLastname(name);
                    contact.setNickname(name);
                    contact.setEmail("email");
                    contact.setTelephone(number);
                    if (!contactslist.contains(contact)) {
                        contactslist.add(contact);
                    }


                }
            } finally {
                cursor.close();
            }
        }

        if (contactslist.size() > 0) {
            importContactsView.getContacts(contactslist);
        } else {
            importContactsView.displayError("No Contacts Found");
        }


    }

    public void pushContacts(ArrayList<ContactsModel> selectedcontactslist) {

        if (selectedcontactslist.size() == 0) {
            importContactsView.displayError("Please Select any Contact");
            return;
        }

        try {
            Gson gson = new Gson();
            String listString = gson.toJson(
                    selectedcontactslist,
                    new TypeToken<ArrayList<ContactsModel>>() {
                    }.getType());

            JSONArray array = new JSONArray(listString);


            for (ContactsModel contact : selectedcontactslist) {
                count++;
                gson = new Gson();
                String request = gson.toJson(contact);
                Log.d("request", request);
                ContactsController.uploadBulkCuustomers(MyApplication.returnInstance(), request,
                        this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void success(String success) {
        importContactsView.showProgressbar(count);


    }

    @Override
    public void fail(String fail) {
        importContactsView.hideProgressbar();

    }

    public void refreshAdapter() {
        count = 0;
        importContactsView.refreshAdapter(contactslist);
    }
}
