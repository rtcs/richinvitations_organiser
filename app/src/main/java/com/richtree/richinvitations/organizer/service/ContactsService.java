package com.richtree.richinvitations.organizer.service;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by admin on 01/12/2018.
 */

public class ContactsService {
    private final ServiceDefinition service;

    public ContactsService(Retrofit retrofit) {
        service = retrofit.create(ServiceDefinition.class);
    }

    public Call<ResponseBody> bulkUploadContacts(final String request) {
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                request);
        final Call<ResponseBody> call = service.bulkUploadContacts(requestBody);
        return call;
    }

    interface ServiceDefinition {
        @Headers("x-api-key:67d40994-d138-11e8-a8d5-f2801f1b9fd167d40994-d138-11e8-a8d5-f2801f1b9fd1")
        @POST("bulk/account/create")
        Call<ResponseBody> bulkUploadContacts(@Body RequestBody contact);

    }
}
