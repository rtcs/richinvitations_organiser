package com.richtree.richinvitations.organizer.interfaces;

import android.widget.CheckBox;

import com.richtree.richinvitations.organizer.model.Guest;


/**
 * Created by Lenovo on 10/2/2018.
 */

public interface CheckClickInterface {

    void clickListener(CheckBox view, Guest guest);
}
