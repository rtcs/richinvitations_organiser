package com.richtree.richinvitations.organizer.view;

import com.richtree.richinvitations.organizer.model.ContactsModel;

import java.util.ArrayList;

/**
 * Created by admin on 30/11/2018.
 */

public interface ImportCsvView {
    void previewContacts(ArrayList<ContactsModel> contactsList);

    void showProgressbar(int count);

    void hideProgressbar();
}
