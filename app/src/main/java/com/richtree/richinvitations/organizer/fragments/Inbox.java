package com.richtree.richinvitations.organizer.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.activities.Contacts;
import com.richtree.richinvitations.organizer.activities.GridViewScrollable;
import com.richtree.richinvitations.organizer.activities.Import;
import com.richtree.richinvitations.organizer.activities.Invitations;
import com.richtree.richinvitations.organizer.adapters.MainGridAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Inbox extends Fragment {
    GridViewScrollable grid_view;


    public Inbox() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_inbox, container, false);

        MainGridAdapter adapter = new MainGridAdapter(getActivity());
        grid_view = (GridViewScrollable) view.findViewById(R.id.gridview);
        grid_view.setNumColumns(2);
        grid_view.setAdapter(adapter);
        grid_view.setExpanded(true);

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;
                switch (i) {
                    case 0:
                        intent = new Intent(getActivity(), Invitations.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getActivity(), Contacts.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(getActivity(), Import.class);
                        startActivity(intent);
                        break;
                }
            }
        });

        return view;
    }

}
