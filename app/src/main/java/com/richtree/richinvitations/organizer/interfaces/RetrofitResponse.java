package com.richtree.richinvitations.organizer.interfaces;

import okhttp3.ResponseBody;

/**
 * Created by admin on 08/12/2018.
 */

public interface RetrofitResponse {
    void returnResponse(ResponseBody responseBody);

    void returnException(Throwable t);
}
