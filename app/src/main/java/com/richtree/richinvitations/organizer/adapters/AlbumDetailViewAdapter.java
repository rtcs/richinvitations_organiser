package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.richtree.richinvitations.organizer.R;

/**
 * Created by richtree on 11/30/2018.
 */

public class AlbumDetailViewAdapter extends BaseAdapter {
    Context context;

    private Integer[] images={R.drawable.loading2,R.drawable.loading2, R.drawable.loading2, R.drawable.loading2, R.drawable.loading2};

    public AlbumDetailViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.topic_grid_item, parent, false);
        }

        View view1 = (View) view.findViewById(R.id.white_view);
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        ImageView profileimage = (ImageView) view.findViewById(R.id.iv_topicimage);

        if (checkBox.isChecked()){
            view1.setVisibility(View.VISIBLE);
        }

        profileimage.setImageResource(images[position]);

        return view;
    }

}
