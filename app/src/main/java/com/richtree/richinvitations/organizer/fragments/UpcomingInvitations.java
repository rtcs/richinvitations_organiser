package com.richtree.richinvitations.organizer.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.UpcomingInvitationsAdapter;
import com.richtree.richinvitations.organizer.databinding.FragmentPastInvitationsBinding;
import com.richtree.richinvitations.organizer.model.Order;
import com.richtree.richinvitations.organizer.presenter.UpcomingInvitationsPresenter;
import com.richtree.richinvitations.organizer.utils.Utils;
import com.richtree.richinvitations.organizer.view.UpcomingInvitationsview;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingInvitations extends Fragment implements UpcomingInvitationsview {
    FragmentPastInvitationsBinding binding;
    View view;
    UpcomingInvitationsPresenter upcomingInvitationsPresenter;
    UpcomingInvitationsAdapter adapter;

    /*Commit Cmpleted*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_past_invitations, container, false);
        view = binding.getRoot();
        upcomingInvitationsPresenter = new UpcomingInvitationsPresenter(this);
        upcomingInvitationsPresenter.getUpcomingInvitations();
        return view;
    }


    @Override
    public void displayError(String error) {
        Utils.showToast(error);

    }

    @Override
    public void success(String success) {

    }


    @Override
    public void getUpcomingInvitations(ArrayList<Order> future_invitations) {
        if (future_invitations.size() > 0) {
            adapter = new UpcomingInvitationsAdapter(getActivity(), future_invitations);
            binding.invite.setAdapter(adapter);
        }
    }
}
