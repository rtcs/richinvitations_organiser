package com.richtree.richinvitations.organizer.view;

import com.richtree.richinvitations.organizer.model.ContactsModel;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public interface ImportContactsView extends BaseView {
    void getContacts(ArrayList<ContactsModel> contactslist);

    void showProgressbar(int count);

    void hideProgressbar();


    void refreshAdapter(ArrayList<ContactsModel> contactslist);


}
