package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.databinding.InvitationsListItemBinding;
import com.richtree.richinvitations.organizer.model.Order;

import java.util.ArrayList;

/**
 * Created by richtree on 11/14/2018.
 */

public class PastInvitationsAdapter extends BaseAdapter {


    Context context;
    InvitationsListItemBinding binding;
    ArrayList<Order> orderList = new ArrayList<>();


    public PastInvitationsAdapter(Context context, ArrayList<Order> order_List) {
        this.context = context;
        this.orderList = order_List;


    }


    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.invitations_list_item,
                    null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (InvitationsListItemBinding) convertView.getTag();
        }

        binding.setOrder(orderList.get(position));


        Glide.with(context).load(orderList.get(position).getProduct().getImage())
                .centerCrop()/*.placeholder(R.drawable.loading)*/.crossFade().thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.thumbnail);


        return binding.getRoot();
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }


}
