package com.richtree.richinvitations.organizer.presenter;

import android.util.Log;

import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.view.ContactCheckedCallBack;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public class ContactSelectionPresenter {
    ContactCheckedCallBack contactCheckedCallBack;
    ArrayList<ContactsModel> selectedcontactslist = new ArrayList<>();

    public ContactSelectionPresenter(ContactCheckedCallBack contactCheckedCallBack) {
        this.contactCheckedCallBack = contactCheckedCallBack;
    }

    public void addOrRemoveContact(ContactsModel contact) {

        if (selectedcontactslist.contains(contact)) {
            int pos_to_remove = selectedcontactslist.indexOf(contact);
            selectedcontactslist.remove(pos_to_remove);

            contactCheckedCallBack.iscontactChecked(false);
        } else {
            selectedcontactslist.add(contact);
            contactCheckedCallBack.iscontactChecked(false);

        }

        contactCheckedCallBack.SelectedContacts(selectedcontactslist);


        Log.d("count", String.valueOf(selectedcontactslist.size()));

    }
}
