package com.richtree.richinvitations.organizer.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;

/**
 * Created by admin on 08/12/2018.
 */

public class OrderService {

    private final ServiceDefinition service;

    public OrderService(Retrofit retrofit) {
        service = retrofit.create(OrderService.ServiceDefinition.class);
    }

    public Call<ResponseBody> getPastOrders() {
        final Call<ResponseBody> call = service.getPastOrders();
        return call;
    }


    public Call<ResponseBody> getFutureInvitations() {
        final Call<ResponseBody> call = service.getFutureInvitations();
        return call;
    }


    interface ServiceDefinition {
        @GET("customerorders/past")
        Call<ResponseBody> getPastOrders();

        @GET("customerorders/future")
        Call<ResponseBody> getFutureInvitations();

    }
}
