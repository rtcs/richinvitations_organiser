package com.richtree.richinvitations.organizer.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.interfaces.CheckClickInterface;
import com.richtree.richinvitations.organizer.model.Guest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by richtree on 11/20/2018.
 */

public class GuestListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Guest> guestItems;
    boolean[] itemChecked;
    ArrayList<Guest> attachList = new ArrayList<Guest>();

    public CheckClickInterface checkClickInterface;

    public GuestListAdapter(Activity activity, List<Guest> guestItems, CheckClickInterface checkClickInterface) {
        this.activity = activity;
        this.guestItems = guestItems;
        this.checkClickInterface = checkClickInterface;
        //itemChecked = new boolean[guestItems.size()];

    }
    @Override
    public int getCount() {
        return guestItems.size();
    }

    @Override
    public Object getItem(int location) {
        return guestItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.add_guest_list_item, null);

        /*NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.contactImage);*/

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView phoneNumber = (TextView) convertView.findViewById(R.id.number);
        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

        final Guest guest = guestItems.get(position);

        if (guest.getImage() != null && guest.getImage().length() > 0) {
            imageView.setImageBitmap(StringToBitMap(guest.getImage()));
        }
        name.setText(guest.getName());
        phoneNumber.setText(guest.getPhoneNumber());


        checkBox.setChecked(false);

        if (guest.isChecked()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkClickInterface.clickListener(checkBox, guest);

            }
        });

        return convertView;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
