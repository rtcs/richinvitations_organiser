package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;

import java.util.ArrayList;

/**
 * Created by richtree on 10/5/2018.
 */

public class VideosAdapter extends BaseAdapter {

    Context context;

    private String[] names = {"Tarun Weds Payal Reception"};
    private Integer[] images={R.drawable.no_image};

    public VideosAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.videos_youtube_item_view, parent, false);
        }

        TextView textview_name = (TextView) view.findViewById(R.id.title);
        ImageView profileimage = (ImageView) view.findViewById(R.id.thumbnail);

        textview_name.setText(names[position]);
        profileimage.setImageResource(images[position]);

        return view;
    }

}
