package com.richtree.richinvitations.organizer.activities;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.google.gson.Gson;
import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.controllers.RetrofitController;
import com.richtree.richinvitations.organizer.interfaces.NewBaseRetrofitCallback;
import com.richtree.richinvitations.organizer.model.App;
import com.richtree.richinvitations.organizer.model.User;
import com.richtree.richinvitations.organizer.service.ContactsService;
import com.richtree.richinvitations.organizer.service.OrderService;
import com.richtree.richinvitations.organizer.service.RetrofitService;
import com.richtree.richinvitations.organizer.service.UserService;
import com.richtree.richinvitations.organizer.utils.TinyDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by TGT on 11/9/2017.
 */

public class MyApplication extends Application {

    protected MyApplication application;
    private static MyApplication sInstance = null;
    private static final int RC_SIGN_IN_FB = 1916;
    public UserService userService;
    public ContactsService contactsService;
    public RetrofitService retrofitService;
    public OrderService orderService;

    private static OkHttpClient.Builder httpClient;
    private static Context context;
    MyApplication myapplication;

    ProgressDialog dialog;
    TinyDB tinyDB;

    // Updated your class body:
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        MyApplication.context = getApplicationContext();
        tinyDB = new TinyDB(getBaseContext());
        myapplication = returnInstance();
        dialog = new ProgressDialog(getBaseContext());
        retrofitInit();
        tinyDB = new TinyDB(getApplicationContext());

        // Initialize the SDK before executing any other operations,


    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    public static MyApplication returnInstance() {
        return sInstance;
    }


    public void retrofitInit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.hostnameVerifier((hostname, session) -> true);
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("x-api-key", "2090f06a-b47d-11e8-96f8-529269fb14592090f06a-b47d-11e8-96f8-529269fb1459")
                    .header("x-app-id", " 1")
                    .header("x-access-token", tinyDB.getString("access_token"))
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        OkHttpClient client = httpClient.build();
        Retrofit api = new Retrofit
                .Builder()
                .baseUrl(getResources().getString(R.string.api_base_url))
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

                .build();
        userService = new UserService(api);
        retrofitService = new RetrofitService(api);
        contactsService = new ContactsService(api);
        orderService = new OrderService(api);
    }


    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }


    public class TokenAuthenticator implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());
            if (response.code() == 403) {
                User user = tinyDB.getObject("user", User.class);
                JSONObject object = new JSONObject();
                try {
                    object.put("email", user.getEmail());
                    object.put("password", "user@123");

                    RetrofitController.postRequestController(returnInstance(), dialog, object.toString()
                            , "login", "", new NewBaseRetrofitCallback() {
                                @Override
                                public void Success(String success) {
                                    dialog.dismiss();
                                    try {
                                        JSONObject responseobject = new JSONObject(success);
                                        if (responseobject.getBoolean("success")) {
                                            tinyDB.putString("accesstoken", responseobject.getString
                                                    ("accessToken"));
                                            JSONObject dataobject = responseobject.getJSONObject("data");
                                            User user = new Gson().fromJson(dataobject.toString(), User.class);
                                            App app = new Gson().fromJson(dataobject.getJSONObject("app").toString(),
                                                    App
                                                            .class);
                                            user.setApp(app);
                                            tinyDB.putObject("user", user);

                                        } else {
                                            Toast.makeText(getApplicationContext(), responseobject.getString
                                                            ("message"),
                                                    Toast
                                                            .LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {

            }
            return response;
        }
    }
}