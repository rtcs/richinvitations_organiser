package com.richtree.richinvitations.organizer.utils;

import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.interfaces.BaseRetrofitCallback;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 01/12/2018.
 */

public class ResponseCallback implements Callback<ResponseBody> {
    MyApplication myapplication;
    BaseRetrofitCallback baseRetrofitCallback;

    public ResponseCallback(MyApplication myapplication, BaseRetrofitCallback
            baseRetrofitCallback) {
        this.myapplication = myapplication;
        this.baseRetrofitCallback = baseRetrofitCallback;

    }


    @Override
    public void onFailure(Call call, Throwable t) {

        baseRetrofitCallback
                .fail(toString());

        if (t instanceof IOException) {

        } else {

        }
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            try {
                baseRetrofitCallback.success(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}