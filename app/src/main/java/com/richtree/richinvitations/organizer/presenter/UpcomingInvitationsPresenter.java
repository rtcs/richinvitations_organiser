package com.richtree.richinvitations.organizer.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.richtree.richinvitations.organizer.controllers.OrderController;
import com.richtree.richinvitations.organizer.model.Order;
import com.richtree.richinvitations.organizer.utils.APIResponse;
import com.richtree.richinvitations.organizer.view.UpcomingInvitationsview;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 09/12/2018.
 */

public class UpcomingInvitationsPresenter implements APIResponse {
    UpcomingInvitationsview upcomingInvitationsview;

    public UpcomingInvitationsPresenter(UpcomingInvitationsview upcomingInvitationsview) {
        this.upcomingInvitationsview = upcomingInvitationsview;
    }

    public void getUpcomingInvitations() {
        OrderController.getUpcomingInvitations(this);

    }

    @Override
    public void success(String response) {

        Type order_type = new TypeToken<List<Order>>
                () {
        }.getType();
        Gson gson = new Gson();

        List<Order> orderlist = gson.fromJson(response
                ,
                order_type);
        ArrayList<Order> futureorderslist = new ArrayList<Order>(orderlist);
        upcomingInvitationsview.getUpcomingInvitations(futureorderslist);


    }

    @Override
    public void fail(String error) {
        upcomingInvitationsview.displayError(error);

    }

    @Override
    public void accessTokenExpired() {
        getUpcomingInvitations();

    }
}

