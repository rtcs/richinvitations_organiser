package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;

/**
 * Created by admin on 21/09/2018.
 */

public class ImportContactAdapter extends BaseAdapter {
    Context context;

    String names[] = {"Import from CSV File", "Import from Phone"};
    @Override
    public int getCount() {
        return names.length;
    }

    public ImportContactAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            view = inflater.inflate(R.layout.importcontact_list_item, parent, false);
            TextView name = (TextView) view.findViewById(R.id.tv_name);

            name.setText(names[position]);

        }
        return view;
    }
}
