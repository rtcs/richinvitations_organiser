package com.richtree.richinvitations.organizer.fragments;

import android.app.ListFragment;
import android.widget.PopupMenu;
import android.view.View;

import com.richtree.richinvitations.organizer.R;

/**
 * Created by richtree on 11/17/2018.
 */

public class CustomFragment extends ListFragment implements View.OnClickListener {

    @Override
    public void onClick(final View v) {
        v.post(new Runnable() {
            @Override
            public void run() {
                showPopupMenu(v);
            }
        });
    }

    private void showPopupMenu(View view) {

        PopupMenu popup = new PopupMenu(getActivity(), view);

        popup.getMenuInflater().inflate(R.menu.invitation_popup, popup.getMenu());

        popup.show();
    }
}
