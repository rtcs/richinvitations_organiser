package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.activities.InvitationDetailsEdit;


/**
 * Created by richtree on 11/14/2018.
 */

public class UpcomingAdapter extends BaseAdapter implements Filterable {
    Context context;
    ImageView imageView;

    private String[] names = {"Tarun And Payal Wedding Anniversary", "Vyas Family House Warming Seceromany"};
    private Integer[] images = {R.drawable.no_image, R.drawable.no_image};

    public UpcomingAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.invitations_list_item, parent, false);
        }

        imageView = (ImageView) view.findViewById(R.id.menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, imageView);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.invitation_popup, popup.getMenu());

                //registering invitation_popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();

                        if (id == R.id.invite){
                            Toast.makeText(context, "Invite was Cliked", Toast.LENGTH_SHORT).show();
                        }

                        if (id == R.id.edit) {
                            Intent intent = new Intent(context, InvitationDetailsEdit.class);
                            context.startActivity(intent);
                        }

                        if (id == R.id.delete) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setCancelable(false);
                            builder.setTitle("Delete");
                            builder.setMessage("Do you want to delete this invitation ? Please Confirm");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Ok was Cliked", Toast.LENGTH_SHORT).show();
                                }
                            })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Toast.makeText(context, "Cancel was Cliked", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            builder.create().show();
                        }
                        return false;
                    }
                });

                popup.show();//showing invitation_popup menu
            }
        });

        TextView textview_name = (TextView) view.findViewById(R.id.title);
        ImageView profileimage = (ImageView) view.findViewById(R.id.image);

        textview_name.setText(names[position]);
        profileimage.setImageResource(images[position]);

        return view;
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
