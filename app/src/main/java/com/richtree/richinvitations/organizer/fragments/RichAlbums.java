package com.richtree.richinvitations.organizer.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.activities.AddAlbumActivity;
import com.richtree.richinvitations.organizer.adapters.AlbumsAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RichAlbums extends Fragment {
    ListView listView;

    public RichAlbums() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_rich_albums, container, false);

        listView = (ListView) view.findViewById(R.id.albums);
        final AlbumsAdapter adapter = new AlbumsAdapter(getActivity());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Intent intent = new Intent(getActivity(), InvitationDetails.class);
                //startActivity(intent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddAlbumActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

}
