package com.richtree.richinvitations.organizer.interfaces;

/**
 * Created by admin on 26/03/2018.
 */

public interface SessionCallback {
    void success(String success);
    void fail(String fail);
}
