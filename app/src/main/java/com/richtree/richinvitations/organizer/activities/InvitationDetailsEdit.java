package com.richtree.richinvitations.organizer.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.richtree.richinvitations.organizer.R;

public class InvitationDetailsEdit extends AppCompatActivity {
    private static final String[] ITEMS = {"WEDDING INVITATION", "BIRTHDAY INVITATION", "PARTY INVITATION", "FESTIVAL INVITATION", "EVENT INVITATION"};
    private ArrayAdapter<String> adapter;
    MaterialSpinner spinner1;
    ImageView camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_details_edit);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit Invitation Details");

        spinner1 = (MaterialSpinner) findViewById(R.id.spinner);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        initSpinnerMultiline();
    }

    private void initSpinnerMultiline() {
        spinner1.setAdapter(adapter);
        spinner1.setHint("Select Category");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onBackPressed() {
        Intent intent = new Intent(this, InvitationDetails.class);
        startActivity(intent);
        finish();
    }*/
}
