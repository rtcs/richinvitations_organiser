package com.richtree.richinvitations.organizer.utils;

/**
 * Created by admin on 08/12/2018.
 */

public class CONSTANTS {
    public static final String INVALID_PHONE_FORMAT = "Invalid Phone Number";
    public static final String INVALID_EMAIL_FORMAT = "Invalid Email";
    public static final String EMPTY_FIELDS = "Please Enter the Fields";
    public static final String API_ERROR = "Something Went Wrong";
    public static final String NO_INTERNET = "No Network Connection";
    public static final String PHONE_NUMBER_REGEX = "^[7-9][0-9]{9}$";

}
