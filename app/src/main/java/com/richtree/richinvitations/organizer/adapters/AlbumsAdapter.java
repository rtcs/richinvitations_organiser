package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.richtree.richinvitations.organizer.R;

/**
 * Created by richtree on 11/16/2018.
 */

public class AlbumsAdapter extends BaseAdapter {
    Context context;

    private String[] names = {"Madhusudhan Vyas Family", "Govindram Ji Upadhyay - Mayara Paksh", "RED ROSE PALACE", "Tarun Reception Album 1", "Tarun Reception Album 2"};
    private Integer[] images={R.drawable.no_image,R.drawable.no_image, R.drawable.no_image, R.drawable.no_image, R.drawable.no_image};

    public AlbumsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.album_list_item, parent, false);
        }

        TextView textview_name = (TextView) view.findViewById(R.id.title);
        ImageView profileimage = (ImageView) view.findViewById(R.id.image);

        textview_name.setText(names[position]);
        profileimage.setImageResource(images[position]);

        return view;
    }

}
