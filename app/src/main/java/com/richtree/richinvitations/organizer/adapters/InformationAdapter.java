package com.richtree.richinvitations.organizer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.richtree.richinvitations.organizer.R;

/**
 * Created by richtree on 11/20/2018.
 */

public class InformationAdapter extends BaseAdapter {
    Context context;
    ImageView imageView;

    private String[] organizer = {"Mayara Paksh - मायरा पक्ष:", "Our Firms - प्रथिस्थान", "BRIDE GROOM : वर पक्ष", "VENUE - कार्यक्रम स्थान"};
    private String[] address = {
            "श्रीमान जवहारलालजी,श्री केवलचंदजी,श्रीमनलालजी,श्री इन्द्रचंदजी,श्रीअशोकजी ,श्रीपंकजजी,श्री जी जितन्द्रजी,श्री अंशुल एवम्र समस्त सालेचा परिवर\n" +
                    "इन्दोर(पाली)",
            "M/S. GURU AND COMPANY\n" +
                    "M/S. VIJAY SHANTHI ELECTRICALS PVT. LTD.\n" +
                    "\n" +
                    "4-4-460, Kundaswamy Lane,\n" +
                    "Sultan Bazaar, Hyderabad - 500095\n" +
                    "Ph: +91-40-29805400, 2475 5405, 24760162",
            "श्रीमान सा. श्रीमाणकचंदजी,श्री शान्तीलालजी,श्री मोतीलालजी,श्रीजयंतीलाजी,डा.जवहारलालजी, श्री हमराजजी,श्रीमनोजजी,श्री रीशभजी,श्रीनिखीलजी,श्रीसौरभजी, श्रीनितीनकमरजी,\n" +
            "एवाम समस्त लोढा परिवर,बेगलुरु (धीनावासा)",
            "HOTEL PARK\n" +
                    "Address: 22, Raj Bhavan Road, Somajiguda, Hyderabad, Telangana 500082\n" +
                    "Phone: 040 2345 6789"};

    public InformationAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return organizer.length;
    }

    @Override
    public Object getItem(int position) {
        return organizer[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.information_list_item_attributes, parent, false);
        }

        imageView = (ImageView) view.findViewById(R.id.menu);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, imageView);

                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.information_popup, popup.getMenu());

                //registering invitation_popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(context,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing invitation_popup menu
            }
        });

        TextView event_or = (TextView) view.findViewById(R.id.event_organizer);
        TextView event_ad = (TextView) view.findViewById(R.id.event_address);

        event_or.setText(organizer[position]);
        event_ad.setText(address[position]);

        return view;
    }

}
