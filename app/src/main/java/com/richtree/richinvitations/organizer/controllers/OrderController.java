package com.richtree.richinvitations.organizer.controllers;

import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.utils.APIResponse;
import com.richtree.richinvitations.organizer.utils.RetrofitCallBack;

/**
 * Created by admin on 08/12/2018.
 */

public class OrderController {
    public static void getPastOrders(APIResponse apiResponse) {
        MyApplication.returnInstance().orderService.getPastOrders().enqueue(new RetrofitCallBack
                (apiResponse));

    }

    public static void getUpcomingInvitations(APIResponse apiResponse) {
        MyApplication.returnInstance().orderService.getFutureInvitations().enqueue(new RetrofitCallBack
                (apiResponse));

    }
}
