package com.richtree.richinvitations.organizer.utils;

import android.widget.Toast;

import com.richtree.richinvitations.organizer.activities.MyApplication;

/**
 * Created by admin on 08/12/2018.
 */

public class Utils {
    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPhoneNumberValid(String phonenumber) {
        return phonenumber.matches(CONSTANTS.PHONE_NUMBER_REGEX);

    }

    public static String isInputValid(String input) {

        if (!input.isEmpty()) {
            if (input.contains("@")) {
                if (!isEmailValid(input))
                    return CONSTANTS.INVALID_EMAIL_FORMAT;

            } else {
                if (!isPhoneNumberValid(input))
                    return CONSTANTS.INVALID_PHONE_FORMAT;
            }
        } else return CONSTANTS.EMPTY_FIELDS;


        return "";
    }

    public static String isPhonenumberOrEmail(String username) {
        if (username.contains("@")) {
            return username;
        } else {
            return username + "@richevents.in";
        }


    }

    public static void showToast(String message) {
        Toast.makeText(MyApplication.returnInstance(), message, Toast.LENGTH_SHORT).show();
    }


}