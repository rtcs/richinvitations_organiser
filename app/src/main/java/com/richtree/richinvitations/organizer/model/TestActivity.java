package com.richtree.richinvitations.organizer.model;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 30/11/2018.
 */

public class TestActivity extends AppCompatActivity {
    int PICKFILE_RESULT_CODE = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, PICKFILE_RESULT_CODE);




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String[] paths = new File(data.getData().getPath()).getAbsolutePath().split(":");
        String filename = paths[1];
        File file = new File(Environment.getExternalStorageDirectory() + "/" + filename);
        try {

            List<Map<?, ?>> mapList = readObjectsFromCsv(file);
            try {
                Gson gson = new Gson();
                JSONArray array = new JSONArray(mapList.toString());
                Type type = new TypeToken<List<ContactsModel>>() {
                }.getType();
                List<ContactsModel> contactList = gson.fromJson(array.toString(), type);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }






      /*  CSVReader reader = null;
        try {
            CsvToBean<ImportContactsModel> csvToBean = new CsvToBean<ImportContactsModel>();
            HeaderColumnNameTranslateMappingStrategy<ImportContactsModel> strategy =
                    new HeaderColumnNameTranslateMappingStrategy<ImportContactsModel>();
            strategy.setType(ImportContactsModel.class);
            reader = new CSVReader(new FileReader(file));
            List<ImportContactsModel> list = csvToBean.parse(strategy, new CSVReader(reader));
            for (ImportContactsModel o : list) {

            }



            *//*try {
                List myEntries = reader.readAll();
            } catch (IOException e) {
                e.printStackTrace();
            }*//*
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;

        Cursor returnCursor =
                getContentResolver().query(contentURI, null, null, null, null);
    /*
     * Get the column indexes of the data in the Cursor,
     * move to the first row in the Cursor, get the data,
     * and display it.
     */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        result = returnCursor.getString(nameIndex);
        return result;
    }

    public List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {
        CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
        CsvMapper csvMapper = new CsvMapper();
        MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);

        return mappingIterator.readAll();
    }

    public void writeAsJson(List<Map<?, ?>> data, File file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, data);
    }
}
