package com.richtree.richinvitations.organizer.model.ViewModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 08/12/2018.
 */

public class User {
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("customer_group_id")
    @Expose
    private Integer customerGroupId;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("approved")
    @Expose
    private Integer approved;
    @SerializedName("safe")
    @Expose
    private Integer safe;
    @SerializedName("cart")
    @Expose
    private Object cart;
    @SerializedName("wishlist")
    @Expose
    private Object wishlist;
    @SerializedName("newsletter")
    @Expose
    private Integer newsletter;
    @SerializedName("address_id")
    @Expose
    private Integer addressId;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;
    @SerializedName("account_custom_field")
    @Expose
    private Object accountCustomField;
    @SerializedName("app")
    @Expose
    private App app;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(Integer customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public Integer getSafe() {
        return safe;
    }

    public void setSafe(Integer safe) {
        this.safe = safe;
    }

    public Object getCart() {
        return cart;
    }

    public void setCart(Object cart) {
        this.cart = cart;
    }

    public Object getWishlist() {
        return wishlist;
    }

    public void setWishlist(Object wishlist) {
        this.wishlist = wishlist;
    }

    public Integer getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Integer newsletter) {
        this.newsletter = newsletter;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Object getAccountCustomField() {
        return accountCustomField;
    }

    public void setAccountCustomField(Object accountCustomField) {
        this.accountCustomField = accountCustomField;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }


    public  class App {
        @SerializedName("store_id")
        @Expose
        private Integer storeId;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("device_platform")
        @Expose
        private String devicePlatform;
        @SerializedName("device_platform_version")
        @Expose
        private String devicePlatformVersion;
        @SerializedName("device_model")
        @Expose
        private String deviceModel;
        @SerializedName("notification_token")
        @Expose
        private String notificationToken;
        @SerializedName("notification_status")
        @Expose
        private Integer notificationStatus;
        @SerializedName("terms_conditions_agreed")
        @Expose
        private Integer termsConditionsAgreed;

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDevicePlatform() {
            return devicePlatform;
        }

        public void setDevicePlatform(String devicePlatform) {
            this.devicePlatform = devicePlatform;
        }

        public String getDevicePlatformVersion() {
            return devicePlatformVersion;
        }

        public void setDevicePlatformVersion(String devicePlatformVersion) {
            this.devicePlatformVersion = devicePlatformVersion;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getNotificationToken() {
            return notificationToken;
        }

        public void setNotificationToken(String notificationToken) {
            this.notificationToken = notificationToken;
        }

        public Integer getNotificationStatus() {
            return notificationStatus;
        }

        public void setNotificationStatus(Integer notificationStatus) {
            this.notificationStatus = notificationStatus;
        }

        public Integer getTermsConditionsAgreed() {
            return termsConditionsAgreed;
        }

        public void setTermsConditionsAgreed(Integer termsConditionsAgreed) {
            this.termsConditionsAgreed = termsConditionsAgreed;
        }


    }
}
