package com.richtree.richinvitations.organizer.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.richtree.richinvitations.organizer.BR;

/**
 * Created by admin on 08/12/2018.
 */

public class Order {
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;
    @SerializedName("product")
    @Expose
    private Order_Product product;
    @SerializedName("seller")
    @Expose
    private Order_Seller seller;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Order_Product getProduct() {
        return product;
    }

    public void setProduct(Order_Product product) {
        this.product = product;
    }

    public Order_Seller getSeller() {
        return seller;
    }

    public void setSeller(Order_Seller seller) {
        this.seller = seller;
    }

    public class Order_Product extends BaseObservable {
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("end_date")
        @Expose
        private String endDate;

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getName() {
            return name;
        }

        @Bindable
        public void setName(String name) {
            this.name = name;
            notifyPropertyChanged(BR.name);
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        @Bindable
        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
            notifyPropertyChanged(BR.image);
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        @Bindable
        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
            notifyPropertyChanged(BR.startDate);
        }

        @Bindable
        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
            notifyPropertyChanged(BR.endDate);
        }
    }

    public class Order_Seller {
        @SerializedName("seller_id")
        @Expose
        private Integer sellerId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("avatar")
        @Expose
        private Object avatar;
        @SerializedName("banner")
        @Expose
        private Object banner;
        @SerializedName("group_name")
        @Expose
        private String groupName;
        @SerializedName("group_description")
        @Expose
        private String groupDescription;

        public Integer getSellerId() {
            return sellerId;
        }

        public void setSellerId(Integer sellerId) {
            this.sellerId = sellerId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public Object getBanner() {
            return banner;
        }

        public void setBanner(Object banner) {
            this.banner = banner;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupDescription() {
            return groupDescription;
        }

        public void setGroupDescription(String groupDescription) {
            this.groupDescription = groupDescription;
        }
    }


}
