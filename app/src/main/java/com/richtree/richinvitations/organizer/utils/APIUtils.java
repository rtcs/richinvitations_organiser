package com.richtree.richinvitations.organizer.utils;

import com.google.gson.Gson;
import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.model.ViewModel.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by admin on 08/12/2018.
 */

public class APIUtils {

    public static boolean checkIfJsonEmpty(String json) {
        if (json.isEmpty()) {

            return true;
        } else return false;
    }

    public static void getRetrofitStatus(Response<ResponseBody> response, APIResponse
            apiResponse) {

        if (response.isSuccessful())
            try {
                String jsonresponse = response.body().string();
                BaseResponse baseResponse = getBaseResponseObject(jsonresponse);
                storeAccessToken(baseResponse.getAccessToken());
                if (checkApiStatus(baseResponse, apiResponse))
                    apiResponse.success(getSuccessResponse(jsonresponse));
                else apiResponse.fail(baseResponse.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }

    }


    public static void storeAccessToken(String token) {
        TinyDB tinyDB = new TinyDB(MyApplication.returnInstance());
        if (tinyDB.getString("access_token").equals(null) || tinyDB.getString("access_token").isEmpty())
            tinyDB.putString("access_token", token);
        return;


    }

    public static void getRetrofitStatus(Throwable throwable, APIResponse
            apiResponse) {
        if (throwable instanceof IOException)
            apiResponse.fail(CONSTANTS.NO_INTERNET);
        else apiResponse.fail(CONSTANTS.API_ERROR);

    }

    public static Object convertJsonToObject(String json, Object object) {
        Gson gson = new Gson();
        object = gson.fromJson(json, object.getClass());
        return object;

    }

    public static BaseResponse getBaseResponseObject(String response) {
        Object object = APIUtils.convertJsonToObject(response, new BaseResponse());
        BaseResponse baseResponse = (BaseResponse) object;
        return baseResponse;

    }


    public static boolean checkApiStatus(BaseResponse baseResponse, APIResponse apiResponse) {
        if (baseResponse.getSuccess()) {
            if (baseResponse.getCode() == 100) {
                return true;
            } else if (baseResponse.getCode() == 407) {
                apiResponse.accessTokenExpired();
                return false;
            }

        } else return false;

        return false;
    }

    public static String getSuccessResponse(String json) {
        JSONObject jsonobject = null;
        String finalobject = null;
        try {
            jsonobject = new JSONObject(json);
            if (jsonobject.has("data")) {
                JSONObject dataObject = jsonobject.optJSONObject("data");

                if (dataObject != null) {
                    finalobject = dataObject.toString();
                } else {
                    JSONArray array = jsonobject.optJSONArray("data");
                    finalobject = array.toString();
                    //Do things with array
                }
            } else {
                // Do nothing or throw exception if "data" is a mandatory field
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


      /*  JSONObject object = null;
        JSONArray jsonArray = null;

        try {
            Object ob = new JSONTokener(json).nextValue();
            if (ob instanceof JSONObject) {
                object = new JSONObject(json).optJSONObject("data");
                finalobject = object.toString();
            } else {
                jsonArray = new JSONObject(json).optJSONArray("data");
                finalobject = jsonArray.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        return finalobject;
    }


}