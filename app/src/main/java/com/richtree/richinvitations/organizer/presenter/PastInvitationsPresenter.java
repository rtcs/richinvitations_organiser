package com.richtree.richinvitations.organizer.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.richtree.richinvitations.organizer.controllers.OrderController;
import com.richtree.richinvitations.organizer.model.Order;
import com.richtree.richinvitations.organizer.utils.APIResponse;
import com.richtree.richinvitations.organizer.view.PastInvitationsView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 08/12/2018.
 */

public class PastInvitationsPresenter implements APIResponse {
    PastInvitationsView pastInvitationsView;

    public PastInvitationsPresenter(PastInvitationsView pastInvitationsView) {
        this.pastInvitationsView = pastInvitationsView;
    }

    public void getPastOrders() {
        OrderController.getPastOrders(this);

    }

    @Override
    public void success(String response) {

        Type order_type = new TypeToken<List<Order>>
                () {
        }.getType();
        Gson gson = new Gson();

        List<Order> orderlist = gson.fromJson(response
                ,
                order_type);
        ArrayList<Order> pastordersList = new ArrayList<Order>(orderlist);
        pastInvitationsView.getPastOrders(pastordersList);


    }

    @Override
    public void fail(String error) {
        pastInvitationsView.displayError(error);

    }

    @Override
    public void accessTokenExpired() {
        getPastOrders();

    }
}
