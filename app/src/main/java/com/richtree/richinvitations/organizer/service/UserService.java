package com.richtree.richinvitations.organizer.service;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by admin on 26/03/2018.
 */

public class UserService {
    private final ServiceDefinition service;

    public UserService(Retrofit retrofit) {
        service = retrofit.create(ServiceDefinition.class);
    }

    public Call<ResponseBody> loginUser(String request) {

        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                request);
        final Call<ResponseBody> call = service.login(requestBody);
        return call;
    }


    interface ServiceDefinition {
        @POST("login")
        Call<ResponseBody> login(@Body RequestBody user);

    }
}
