package com.richtree.richinvitations.organizer.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.SelectedGuestAdapter;
import com.richtree.richinvitations.organizer.interfaces.CheckClickInterface;
import com.richtree.richinvitations.organizer.model.Guest;

import java.util.ArrayList;

public class SelectedGuestList extends AppCompatActivity {
    ListView listView;
    ArrayList<Guest> attachList;
    CheckClickInterface checkClickInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_guest_list);
        setTitle("Selected Guest List");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.selected_guest_list);
        attachList = (ArrayList<Guest>) getIntent().getSerializableExtra("list");
        SelectedGuestAdapter adapter = new SelectedGuestAdapter(SelectedGuestList.this, attachList);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
