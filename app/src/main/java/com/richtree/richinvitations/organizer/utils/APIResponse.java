package com.richtree.richinvitations.organizer.utils;

/**
 * Created by admin on 08/12/2018.
 */

public interface APIResponse {
    void success(String response);

    void fail(String error);

    default void accessTokenExpired() {

    }
}
