package com.richtree.richinvitations.organizer.view;

/**
 * Created by admin on 28/11/2018.
 */


public interface LoginView {


    String getUserName();

    void showError(String error);

    void navigateToMainActivity();

}
