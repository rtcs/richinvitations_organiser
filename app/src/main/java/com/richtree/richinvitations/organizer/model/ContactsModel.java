package com.richtree.richinvitations.organizer.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.richtree.richinvitations.organizer.BR;

import java.io.Serializable;

/**
 * Created by admin on 30/11/2018.
 */

public class ContactsModel extends BaseObservable implements Serializable {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("status")
    @Expose
    private Integer status = 1;
    @SerializedName("approved")
    @Expose
    private Integer approved = 0;
    @SerializedName("safe")
    @Expose
    private Integer safe = 1;
    @SerializedName("store_id")
    @Expose
    private Integer storeId = 1;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId = 1389;
    @SerializedName("customer_group_id")
    @Expose
    private Integer customerGroupId = 2;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(Integer customerGroupId) {
        this.customerGroupId = customerGroupId;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Bindable
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
        notifyPropertyChanged(BR.firstname);
    }

    @Bindable
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
        notifyPropertyChanged(BR.lastname);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Bindable
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
        notifyPropertyChanged(BR.telephone);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public Integer getSafe() {
        return safe;
    }

    public void setSafe(Integer safe) {
        this.safe = safe;
    }

    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        if (o instanceof ContactsModel) {
            return this.telephone.equals(((ContactsModel) o).getTelephone());
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return telephone.hashCode();
    }
}
