package com.richtree.richinvitations.organizer.view;

/**
 * Created by admin on 02/12/2018.
 */

public interface BaseView {

    void displayError(String error);

    void success(String success);
}
