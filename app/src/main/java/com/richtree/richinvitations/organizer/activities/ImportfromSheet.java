package com.richtree.richinvitations.organizer.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.ImportContacts_Sheet_Adapter;


import com.richtree.richinvitations.organizer.databinding.ActivityImportfromSheetBinding;
import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.presenter.CsvImportPresenter;
import com.richtree.richinvitations.organizer.view.ImportCsvView;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ImportfromSheet extends AppCompatActivity implements ImportCsvView, View.OnClickListener {
    int PICKFILE_RESULT_CODE = 100;
    CsvImportPresenter csvImportPresenter;
    ImportContacts_Sheet_Adapter adapter;
    ProgressDialog dialog;
    Button btn_pushcontacts;
    JSONArray csv_to_jsonarray;
    ActivityImportfromSheetBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Import.class);
        startActivity(intent);
        finish();
    }

    public void init() {
        setTitle("Import From Sheet");
        dialog = new ProgressDialog(ImportfromSheet.this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_importfrom_sheet);
        View footerView = ((LayoutInflater) ImportfromSheet.this.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.layout_footer_listview, null, false);


        csvImportPresenter = new CsvImportPresenter(this);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.browse.setOnClickListener(this);
        binding.preview.setOnClickListener(this);
        binding.save.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.browse:
                openFileExplorer();
                break;

            case R.id.preview:
                csvImportPresenter.previewContacts(csv_to_jsonarray);
                break;

            case R.id.save:
                dialog.show();
                csvImportPresenter.pushContacts(MyApplication.returnInstance(), csv_to_jsonarray.toString());
                break;


        }
    }

    @Override
    public void previewContacts(ArrayList<ContactsModel> contactsList) {
        adapter = new ImportContacts_Sheet_Adapter(ImportfromSheet.this, contactsList, false);
        binding.listContacts.setAdapter(adapter);
        binding.listContacts.setVisibility(View.VISIBLE);

    }

    @Override
    public void showProgressbar(int count) {
        if (count == csv_to_jsonarray.length()) {
            Toast.makeText(ImportfromSheet.this, "Saved Successfuly", Toast.LENGTH_SHORT)
                    .show();
            dialog.dismiss();
        }
    }


    @Override
    public void hideProgressbar() {
        dialog.dismiss();
        Toast.makeText(ImportfromSheet.this, "Something went wrong", Toast
                .LENGTH_SHORT)
                .show();

    }

    public void openFileExplorer() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, PICKFILE_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File file = csvImportPresenter.onActivityResult(data, requestCode);
        binding.etFilename.setText(file.getName());
        try {
            csv_to_jsonarray = csvImportPresenter.readObjectsFromCsv(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
