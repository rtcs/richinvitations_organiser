package com.richtree.richinvitations.organizer.utils;


import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 08/12/2018.
 */

public class RetrofitCallBack implements Callback<ResponseBody> {
    APIResponse apiResponse;

    public RetrofitCallBack(APIResponse response) {
        this.apiResponse = response;
    }


    @Override
    public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
        APIUtils.getRetrofitStatus(response, apiResponse);


    }

    @Override
    public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
        APIUtils.getRetrofitStatus(t, apiResponse);
    }
}
