package com.richtree.richinvitations.organizer.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.BottomNavigationViewHelper;
import com.richtree.richinvitations.organizer.fragments.GuestRSVP;
import com.richtree.richinvitations.organizer.fragments.Home;
import com.richtree.richinvitations.organizer.fragments.Guests;
import com.richtree.richinvitations.organizer.fragments.Gallery;
import com.richtree.richinvitations.organizer.fragments.Information;
import com.richtree.richinvitations.organizer.fragments.More;
import com.richtree.richinvitations.organizer.model.User;
import com.richtree.richinvitations.organizer.utils.TinyDB;

public class InvitationDetails extends AppCompatActivity {
    private Toolbar toolbar;
    BottomNavigationView bottomNavigationView;
    TinyDB tinydb;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_details);
        tinydb = new TinyDB(InvitationDetails.this);
        user = tinydb.getObject("user", User.class);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadFragment(new Home());
        setTitle("Home");

        //Initializing the bottomNavigationView
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.home:
                    fragment = new Home();
                    loadFragment(fragment);
                    setTitle("Home");
                    return true;
                case R.id.information:
                    fragment = new Information();
                    loadFragment(fragment);
                    setTitle("Information");
                    return true;
                case R.id.gallery:
                    fragment = new Gallery();
                    loadFragment(fragment);
                    setTitle("Gallery");
                    return true;
                case R.id.guests:
                    fragment = new GuestRSVP();
                    loadFragment(fragment);
                    setTitle("Guest");
                    return true;
                /*case R.id.guests:
                    fragment = new Guests();
                    loadFragment(fragment);
                    setTitle("Invite List");
                    return true;*/
                case R.id.more:
                    fragment = new More();
                    loadFragment(fragment);
                    setTitle("More");
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Invitations.class);
        startActivity(intent);
        finish();
    }
}
