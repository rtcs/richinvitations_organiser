package com.richtree.richinvitations.organizer.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.richtree.richinvitations.organizer.R;

public class AddAlbumActivity extends AppCompatActivity {
    private static final String[] ITEMS = {"ALBUM", "RICH ALBUM", "VIDEO"};
    private ArrayAdapter<String> adapter;
    MaterialSpinner spinner1;
    ImageView camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_album);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add New");

        spinner1 = (MaterialSpinner) findViewById(R.id.spinner);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        camera = (ImageView) findViewById(R.id.upload);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        initSpinnerMultiline();
    }

    private void initSpinnerMultiline() {
        spinner1.setAdapter(adapter);
        spinner1.setHint("Select an Category");
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddAlbumActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    //userChoosenTask = "Take Photo";
                    //pickImage(Sources.CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    //userChoosenTask = "Choose from Library";
                    //pickImage(Sources.GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
