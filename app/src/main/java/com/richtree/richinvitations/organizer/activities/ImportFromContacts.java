package com.richtree.richinvitations.organizer.activities;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.adapters.ImportContacts_Adapter;
import com.richtree.richinvitations.organizer.databinding.ActivityImportFromContactsBinding;
import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.presenter.ImportContactsPresenter;
import com.richtree.richinvitations.organizer.view.ImportContactsView;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public class ImportFromContacts extends AppCompatActivity implements ImportContactsView,
        ImportContacts_Adapter.getSelectedContactsCallback, View.OnClickListener {
    ActivityImportFromContactsBinding binding;
    ImportContactsPresenter importContactsPresenter;
    ProgressDialog dialog;
    ArrayList<ContactsModel> savedcontactslist = new ArrayList<>();
    ArrayList<ContactsModel> contactslist = new ArrayList<>();
    boolean isUploadcomplete = false;
    ImportContacts_Adapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();


    }

    public void init() {

        binding = DataBindingUtil.setContentView(this, R.layout.activity_import_from_contacts);
        importContactsPresenter = new ImportContactsPresenter(this);
        dialog = new ProgressDialog(ImportFromContacts.this
        );

        binding.save.setOnClickListener(this);
        importContactsPresenter.readContacts(getContentResolver());


    }

    @Override
    public void getContacts(ArrayList<ContactsModel> contacts_list) {
        adapter = new ImportContacts_Adapter(ImportFromContacts.this,
                contacts_list, this);
        binding.contactList.setAdapter(adapter);


    }

    @Override
    public void showProgressbar(int count) {

        if (count == savedcontactslist.size()) {

            Toast.makeText(ImportFromContacts.this, "Saved Successfully", Toast.LENGTH_SHORT)
                    .show();
            savedcontactslist.clear();
            binding.total.setText("Total");
            importContactsPresenter.refreshAdapter();


        }


    }

    @Override
    public void hideProgressbar() {
        dialog.dismiss();
        savedcontactslist.clear();
        Toast.makeText(ImportFromContacts.this, "Something Went Wrong", Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    public void refreshAdapter(ArrayList<ContactsModel> contactslist) {
        adapter = new ImportContacts_Adapter(ImportFromContacts.this,
                contactslist, this);
        binding.contactList.setAdapter(adapter);
        isUploadcomplete = false;

    }


    @Override
    public void getSelected(ArrayList<ContactsModel> selectedcontactslist) {
        savedcontactslist = selectedcontactslist;
        if (selectedcontactslist.size() == 0) binding.total.setText("Total");
        else binding.total.setText("Total  :  " + String.valueOf(selectedcontactslist.size()));

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.view:

                break;

            case R.id.save:

                importContactsPresenter.pushContacts(savedcontactslist);

                break;
        }

    }

    @Override
    public void displayError(String error) {
        Toast.makeText(ImportFromContacts.this, error, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void success(String success) {

    }
}
