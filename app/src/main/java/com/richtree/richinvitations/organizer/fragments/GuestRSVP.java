package com.richtree.richinvitations.organizer.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richtree.richinvitations.organizer.R;
import com.richtree.richinvitations.organizer.activities.AddGuestList;

import java.util.ArrayList;
import java.util.List;

public class GuestRSVP extends Fragment {
    View view;
    Gallery.Adapter adapter;
    private Context context;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new Gallery.Adapter(getChildFragmentManager());
        adapter.addFragment(new YesRSVP(), "Yes (3)");
        adapter.addFragment(new MaybeRSVP(), "Maybe (0)");
        adapter.addFragment(new NoRSVP(), "No (0)");
        adapter.addFragment(new NoReplyRSVP(), "No Reply (0)");
        viewPager.setAdapter(adapter);
    }


    public GuestRSVP() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_guest_rsvp, container, false);

        super.onCreate(savedInstanceState);
        final ViewPager viewPager = view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        TabLayout tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddGuestList.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity) {//Name of your activity
            this.context = (AppCompatActivity) context;
        }
    }

}
