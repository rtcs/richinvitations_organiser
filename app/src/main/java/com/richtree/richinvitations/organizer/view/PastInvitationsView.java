package com.richtree.richinvitations.organizer.view;

import com.richtree.richinvitations.organizer.model.Order;

import java.util.ArrayList;

/**
 * Created by admin on 08/12/2018.
 */

public interface PastInvitationsView extends BaseView {
    void getPastOrders(ArrayList<Order> past_orders);


}
