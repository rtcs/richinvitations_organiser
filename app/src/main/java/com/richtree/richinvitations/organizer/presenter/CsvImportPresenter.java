package com.richtree.richinvitations.organizer.presenter;

import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.richtree.richinvitations.organizer.activities.MyApplication;
import com.richtree.richinvitations.organizer.controllers.ContactsController;
import com.richtree.richinvitations.organizer.interfaces.BaseRetrofitCallback;
import com.richtree.richinvitations.organizer.model.ContactsModel;
import com.richtree.richinvitations.organizer.view.ImportCsvView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 30/11/2018.
 */

public class CsvImportPresenter implements BaseRetrofitCallback {
    ImportCsvView csvView;
    int count = 0;
    ArrayList<ContactsModel> contactList = new ArrayList<>();

    public CsvImportPresenter(ImportCsvView importCsvView) {
        this.csvView = importCsvView;

    }


    public File onActivityResult(Intent data, int resultCode) {
        File file = null;
        if (resultCode == 100 && data != null) {
            String[] paths = new File(data.getData().getPath()).getAbsolutePath().split(":");
            String filename = paths[1];
            file = new File(Environment.getExternalStorageDirectory() + "/" + filename);
        }
        return file;
    }


    public JSONArray readObjectsFromCsv(File file) throws IOException {
        CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
        JSONArray cvs_to_jsonarray = null;
        CsvMapper csvMapper = new CsvMapper();
        ArrayList<ContactsModel> contactList = new ArrayList<>();
        MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);
        try {

            cvs_to_jsonarray = new JSONArray(mappingIterator.readAll().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cvs_to_jsonarray;


    }

    public void pushContacts(MyApplication myApplication, String request) {

        try {
            JSONArray array = new JSONArray(request);
            for (int i = 0; i < array.length(); i++) {
                count = i + 1;
                array.getJSONObject(i).put("store_id", 1);
                array.getJSONObject(i).put("status", 1);
                array.getJSONObject(i).put("approved", 0);
                array.getJSONObject(i).put("safe", 1);
                ContactsController.uploadBulkCuustomers(myApplication, array.getJSONObject(i)
                                .toString(),
                        this);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void success(String success) {
        Log.d("response", success);
        csvView.showProgressbar(count);


    }

    @Override
    public void fail(String fail) {
        csvView.hideProgressbar();

    }

    public void previewContacts(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<ContactsModel>>() {
        }.getType();
        contactList = gson.fromJson(jsonArray.toString(), type);
        csvView.previewContacts(contactList);


    }
}
