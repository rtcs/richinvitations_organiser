package com.richtree.richinvitations.organizer.activities;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.richtree.richinvitations.organizer.R;

public class AddVideos extends AppCompatActivity {
    private static final String[] CATEGORY = {"ALBUM", "RICH ALBUM", "VIDEO"};
    private ArrayAdapter<String> adapter;
    MaterialSpinner spinner;
    private static final String[] SOURCE = {"API", "YOUTUBE", "OTHERS"};
    private ArrayAdapter<String> adapter2;
    MaterialSpinner spinner2;
    private static final String[] TYPE = {"Intro", "Wedding", "Reception"};
    private ArrayAdapter<String> adapter3;
    MaterialSpinner spinner3;
    ImageView camera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_videos);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add New");

        spinner = (MaterialSpinner) findViewById(R.id.category);
        spinner2 = (MaterialSpinner) findViewById(R.id.source);
        spinner3 = (MaterialSpinner) findViewById(R.id.type);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CATEGORY);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SOURCE);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TYPE);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        camera = (ImageView) findViewById(R.id.upload);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        initSpinnerMultiline();
        initSpinnerMultiline2();
        initSpinnerMultiline3();

    }

    private void initSpinnerMultiline() {
        spinner.setAdapter(adapter);
        spinner.setHint("Select an Category");
    }

    private void initSpinnerMultiline2() {
        spinner2.setAdapter(adapter2);
        spinner2.setHint("Select an Category");
    }

    private void initSpinnerMultiline3() {
        spinner3.setAdapter(adapter3);
        spinner3.setHint("Select an Category");
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddVideos.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    //userChoosenTask = "Take Photo";
                    //pickImage(Sources.CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    //userChoosenTask = "Choose from Library";
                    //pickImage(Sources.GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
