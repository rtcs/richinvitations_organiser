package com.richtree.richinvitations.organizer.view;

import com.richtree.richinvitations.organizer.model.ContactsModel;

import java.util.ArrayList;

/**
 * Created by admin on 02/12/2018.
 */

public interface ContactCheckedCallBack {
    void iscontactChecked(boolean ischeck);

    void SelectedContacts(ArrayList<ContactsModel> selectedcontactslist);
}
